package com.ujian.starwarsapp.model.adapters;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.ujian.starwarsapp.R;

import java.util.List;

public class LinkAdapter extends RecyclerView.Adapter<LinkAdapter.MyViewHolder> {
    private List<String> links;
    private Context context;
    private Intent intent;


    public LinkAdapter(List<String> links, Context context, Intent intent) {
        this.links = links;
        this.context = context;
        this.intent = intent;

    }


    @NonNull
    @Override
    public LinkAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_links, viewGroup, false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull LinkAdapter.MyViewHolder myViewHolder, int i) {
        final String link = links.get(i);
        myViewHolder.link.setText(link);
        myViewHolder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                intent.putExtra("url", link);
                context.startActivity(intent);
            }
        });


    }


    @Override
    public int getItemCount() {
        return links.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        TextView link;


        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            link = itemView.findViewById(R.id.link);

        }

    }
}
