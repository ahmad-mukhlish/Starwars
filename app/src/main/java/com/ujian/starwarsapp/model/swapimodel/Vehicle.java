package com.ujian.starwarsapp.model.swapimodel;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Vehicle {

    @Expose
    @SerializedName("url")
    private String url;
    @Expose
    @SerializedName("edited")
    private String edited;
    @Expose
    @SerializedName("created")
    private String created;
    @Expose
    @SerializedName("films")
    private List<String> films;
    @Expose
    @SerializedName("pilots")
    private List<String> pilots;
    @Expose
    @SerializedName("vehicle_class")
    private String vehicleClass;
    @Expose
    @SerializedName("consumables")
    private String consumables;
    @Expose
    @SerializedName("cargo_capacity")
    private String cargoCapacity;
    @Expose
    @SerializedName("passengers")
    private String passengers;
    @Expose
    @SerializedName("crew")
    private String crew;
    @Expose
    @SerializedName("max_atmosphering_speed")
    private String maxAtmospheringSpeed;
    @Expose
    @SerializedName("length")
    private String length;
    @Expose
    @SerializedName("cost_in_credits")
    private String costInCredits;
    @Expose
    @SerializedName("manufacturer")
    private String manufacturer;
    @Expose
    @SerializedName("model")
    private String model;
    @Expose
    @SerializedName("name")
    private String name;

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getEdited() {
        return edited;
    }

    public void setEdited(String edited) {
        this.edited = edited;
    }

    public String getCreated() {
        return created;
    }

    public void setCreated(String created) {
        this.created = created;
    }

    public List<String> getFilms() {
        return films;
    }

    public void setFilms(List<String> films) {
        this.films = films;
    }

    public List<String> getPilots() {
        return pilots;
    }

    public void setPilots(List<String> pilots) {
        this.pilots = pilots;
    }

    public String getVehicleClass() {
        return vehicleClass;
    }

    public void setVehicleClass(String vehicleClass) {
        this.vehicleClass = vehicleClass;
    }

    public String getConsumables() {
        return consumables;
    }

    public void setConsumables(String consumables) {
        this.consumables = consumables;
    }

    public String getCargoCapacity() {
        return cargoCapacity;
    }

    public void setCargoCapacity(String cargoCapacity) {
        this.cargoCapacity = cargoCapacity;
    }

    public String getPassengers() {
        return passengers;
    }

    public void setPassengers(String passengers) {
        this.passengers = passengers;
    }

    public String getCrew() {
        return crew;
    }

    public void setCrew(String crew) {
        this.crew = crew;
    }

    public String getMaxAtmospheringSpeed() {
        return maxAtmospheringSpeed;
    }

    public void setMaxAtmospheringSpeed(String maxAtmospheringSpeed) {
        this.maxAtmospheringSpeed = maxAtmospheringSpeed;
    }

    public String getLength() {
        return length;
    }

    public void setLength(String length) {
        this.length = length;
    }

    public String getCostInCredits() {
        return costInCredits;
    }

    public void setCostInCredits(String costInCredits) {
        this.costInCredits = costInCredits;
    }

    public String getManufacturer() {
        return manufacturer;
    }

    public void setManufacturer(String manufacturer) {
        this.manufacturer = manufacturer;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
