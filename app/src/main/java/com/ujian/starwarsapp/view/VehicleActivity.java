package com.ujian.starwarsapp.view;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

import com.ujian.starwarsapp.R;
import com.ujian.starwarsapp.databinding.ActivityVehicleBinding;
import com.ujian.starwarsapp.model.adapters.LinkAdapter;
import com.ujian.starwarsapp.model.swapimodel.Vehicle;
import com.ujian.starwarsapp.viewmodel.VehicleViewModel;

public class VehicleActivity extends AppCompatActivity {

    private VehicleViewModel vehicleViewModel;
    private static final String TAG = "VehicleActivity";
    private ActivityVehicleBinding binding ;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityVehicleBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());
        vehicleViewModel = ViewModelProviders.of(this).get(VehicleViewModel.class);
        observeViewModel();
    }

    private void observeViewModel() {

        java.lang.String alamat = getIntent().getExtras().getString("url");

        vehicleViewModel.getVehicle(alamat).observe(this, new Observer<Vehicle>() {
            @Override
            public void onChanged(final Vehicle vehicle) {

                binding.name.setText(vehicle.getName());
                binding.model.setText(vehicle.getModel());
                binding.manufacturer.setText(vehicle.getManufacturer());
                binding.costInCredits.setText(vehicle.getCostInCredits());
                binding.length.setText(vehicle.getLength());
                binding.maxAtmospheringSpeed.setText(vehicle.getMaxAtmospheringSpeed());
                binding.crew.setText(vehicle.getCrew());
                binding.passangers.setText(vehicle.getPassengers());
                binding.cargoCapacity.setText(vehicle.getCargoCapacity());
                binding.consumables.setText(vehicle.getConsumables());
                binding.vehicleClass.setText(vehicle.getVehicleClass());

                binding.rvPilotsStarship.setLayoutManager(new LinearLayoutManager(VehicleActivity.this));
                binding.rvPilotsStarship.setAdapter(new
                        LinkAdapter(vehicle.getFilms(),
                        VehicleActivity.this,
                        new Intent(VehicleActivity.this, PeopleActivity.class)));

                binding.rvPilotsStarship.setNestedScrollingEnabled(false);

                binding.rvFilmStarship.setLayoutManager(new LinearLayoutManager(VehicleActivity.this));
                binding.rvFilmStarship.setAdapter(new
                        LinkAdapter(vehicle.getFilms(),
                        VehicleActivity.this,
                        new Intent(VehicleActivity.this, FilmActivity.class)));

                binding.rvFilmStarship.setNestedScrollingEnabled(false);


                binding.created.setText(vehicle.getCreated());
                binding.edited.setText(vehicle.getEdited());
                binding.url.setText(vehicle.getUrl());
                binding.url.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(VehicleActivity.this, VehicleActivity.class);
                        intent.putExtra("url",vehicle.getUrl());
                        VehicleActivity.this.startActivity(intent);
                    }
                });

            }
        });

    }
}
