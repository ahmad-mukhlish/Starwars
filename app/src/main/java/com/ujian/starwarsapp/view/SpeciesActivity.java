package com.ujian.starwarsapp.view;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

import com.ujian.starwarsapp.R;
import com.ujian.starwarsapp.databinding.ActivitySpeciesBinding;
import com.ujian.starwarsapp.model.adapters.LinkAdapter;
import com.ujian.starwarsapp.model.swapimodel.Film;
import com.ujian.starwarsapp.model.swapimodel.Species;
import com.ujian.starwarsapp.viewmodel.SpeciesViewModel;

public class SpeciesActivity extends AppCompatActivity {

    private SpeciesViewModel speciesViewModel;
    private static final String TAG = "SpeciesActivity";
    private ActivitySpeciesBinding binding ;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivitySpeciesBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());

        speciesViewModel = ViewModelProviders.of(this).get(SpeciesViewModel.class);
        observeViewModel();
    }

    private void observeViewModel() {

        java.lang.String alamat = getIntent().getExtras().getString("url");
        speciesViewModel.getSpecies(alamat).observe(this, new Observer<Species>() {
            @Override
            public void onChanged(final Species species) {


                binding.name.setText(species.getName());
                binding.classification.setText(species.getClassification());
                binding.designation.setText(species.getDesignation());
                binding.averageHeight.setText(species.getAverageHeight());
                binding.skinColors.setText(species.getSkinColors());
                binding.hairColors.setText(species.getHairColors());
                binding.eyeColors.setText(species.getEyeColors());
                binding.averageLifespan.setText(species.getAverageLifespan());
                binding.homeworld.setText(species.getHomeworld());
                binding.homeworld.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(SpeciesActivity.this, PlanetActivity.class);
                        intent.putExtra("url",species.getHomeworld());
                        SpeciesActivity.this.startActivity(intent);
                    }
                });

                binding.languange.setText(species.getLanguage());


                binding.rvPeopleSpecies.setLayoutManager(new LinearLayoutManager(SpeciesActivity.this));
                binding.rvPeopleSpecies.setAdapter(new
                        LinkAdapter(species.getString(),
                        SpeciesActivity.this,
                        new Intent(SpeciesActivity.this, PeopleActivity.class)));

                binding.rvPeopleSpecies.setNestedScrollingEnabled(false);

                binding.rvFilmsSpecies.setLayoutManager(new LinearLayoutManager(SpeciesActivity.this));
                binding.rvFilmsSpecies.setAdapter(new
                        LinkAdapter(species.getFilms(),
                        SpeciesActivity.this,
                        new Intent(SpeciesActivity.this, FilmActivity.class)));



                binding.created.setText(species.getCreated());
                binding.edited.setText(species.getEdited());
                binding.url.setText(species.getUrl());
                binding.url.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(SpeciesActivity.this, SpeciesActivity.class);
                        intent.putExtra("url",species.getUrl());
                        SpeciesActivity.this.startActivity(intent);
                    }
                });
            }
        });
    }
}
