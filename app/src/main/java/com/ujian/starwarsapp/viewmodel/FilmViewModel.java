package com.ujian.starwarsapp.viewmodel;

import android.app.Application;
import android.os.AsyncTask;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.MutableLiveData;

import com.ujian.starwarsapp.model.swapimodel.Film;
import com.ujian.starwarsapp.model.repositories.AllRepositories;

import java.io.IOException;

public class FilmViewModel extends AndroidViewModel {

    private MutableLiveData<Film> filmMutableLiveData = new MutableLiveData<>();

    public FilmViewModel(@NonNull Application application) {
        super(application);
    }

    public MutableLiveData<Film> getFilm(String string) {
        new FetchFilmTask().execute(string);
        return this.filmMutableLiveData;

    }

    private class FetchFilmTask extends AsyncTask<String, Void, Film> {


        @Override
        protected Film doInBackground(String... strings) {
            try {
                return new AllRepositories().getFilm(strings[0]);
            } catch (IOException e) {
                return null;
            }
        }

        @Override
        protected void onPostExecute(Film film) {
            super.onPostExecute(film);
;            filmMutableLiveData.setValue(film);
        }
    }
}
