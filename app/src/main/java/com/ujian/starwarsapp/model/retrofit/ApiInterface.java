package com.ujian.starwarsapp.model.retrofit;

import com.ujian.starwarsapp.model.swapimodel.BaseResponse;
import com.ujian.starwarsapp.model.swapimodel.Film;
import com.ujian.starwarsapp.model.swapimodel.People;
import com.ujian.starwarsapp.model.swapimodel.Planet;
import com.ujian.starwarsapp.model.swapimodel.Species;
import com.ujian.starwarsapp.model.swapimodel.Starship;
import com.ujian.starwarsapp.model.swapimodel.Vehicle;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Url;

public interface ApiInterface {
    @GET("people/")
    Call<BaseResponse> getBaseResponse();

    @GET
    Call<Film> getFilm(@Url java.lang.String url);

    @GET
    Call<People> getPeople(@Url java.lang.String url);

    @GET
    Call<Planet> getPlanet(@Url java.lang.String url);

    @GET
    Call<Species> getSpecies(@Url java.lang.String url);

    @GET
    Call<Starship> getStarship(@Url java.lang.String url);

    @GET
    Call<Vehicle> getVehicle(@Url java.lang.String url);

}
