package com.ujian.starwarsapp.model.adapters;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.ujian.starwarsapp.R;
import com.ujian.starwarsapp.model.swapimodel.People;
import com.ujian.starwarsapp.view.PeopleActivity;

import java.util.List;

public class PeopleAdapter extends RecyclerView.Adapter<PeopleAdapter.MyViewHolder> {
    private List<People> peopleList;
    private Context context;


    public PeopleAdapter(List<People> peopleList, Context context) {
        this.peopleList = peopleList;
        this.context = context;
    }


    @NonNull
    @Override
    public PeopleAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_people, viewGroup, false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull PeopleAdapter.MyViewHolder myViewHolder, int i) {
        final People people = peopleList.get(i);
        myViewHolder.peopleName.setText(people.getName());
        myViewHolder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, PeopleActivity.class);
                intent.putExtra("url",people.getUrl());
                context.startActivity(intent);
            }
        });


    }


    @Override
    public int getItemCount() {
        return peopleList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        TextView peopleName;


        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            peopleName = itemView.findViewById(R.id.people_name);

        }

    }
}
