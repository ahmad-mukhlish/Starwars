package com.ujian.starwarsapp.model.repositories;

import com.ujian.starwarsapp.model.swapimodel.BaseResponse;
import com.ujian.starwarsapp.model.swapimodel.Film;
import com.ujian.starwarsapp.model.swapimodel.People;
import com.ujian.starwarsapp.model.swapimodel.Planet;
import com.ujian.starwarsapp.model.swapimodel.Species;
import com.ujian.starwarsapp.model.swapimodel.Starship;
import com.ujian.starwarsapp.model.swapimodel.Vehicle;

import java.io.IOException;

public class AllRepositories extends BaseRepository{

    public BaseResponse getBaseResponse() throws IOException {
        return network.getBaseResponse().execute().body();
    }

    public Film getFilm(java.lang.String string) throws IOException {
        return network.getFilm(string).execute().body();
    }

    public People getPeople(java.lang.String string) throws IOException {
        return network.getPeople(string).execute().body();
    }


    public Planet getPlanet(java.lang.String string) throws IOException {
        return network.getPlanet(string).execute().body();
    }

    public Species getSpecies(java.lang.String string) throws IOException {
        return network.getSpecies(string).execute().body();
    }

    public Starship getStarship(java.lang.String string) throws IOException {
        return network.getStarship(string).execute().body();
    }

    public Vehicle getVehicle(java.lang.String string) throws IOException {
        return network.getVehicle(string).execute().body();
    }



}
