package com.ujian.starwarsapp.viewmodel;

import android.app.Application;
import android.os.AsyncTask;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.MutableLiveData;

import com.ujian.starwarsapp.model.repositories.AllRepositories;
import com.ujian.starwarsapp.model.swapimodel.People;

import java.io.IOException;

public class PeopleViewModel extends AndroidViewModel {

    private MutableLiveData<People> peopleMutableLiveData = new MutableLiveData<>();

    public PeopleViewModel(@NonNull Application application) {
        super(application);
    }

    public MutableLiveData<People> getPeople(java.lang.String string) {
        new FetchPeopleTask().execute(string);
        return this.peopleMutableLiveData;

    }

    private class FetchPeopleTask extends AsyncTask<java.lang.String, Void, People> {


        @Override
        protected People doInBackground(java.lang.String... strings) {
            try {
                return new AllRepositories().getPeople(strings[0]);
            } catch (IOException e) {
                return null;
            }
        }

        @Override
        protected void onPostExecute(People people) {
            super.onPostExecute(people);
;            peopleMutableLiveData.setValue(people);
        }
    }
}
