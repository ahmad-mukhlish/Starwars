package com.ujian.starwarsapp.viewmodel;

import android.app.Application;
import android.os.AsyncTask;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.MutableLiveData;

import com.ujian.starwarsapp.model.repositories.AllRepositories;
import com.ujian.starwarsapp.model.swapimodel.Starship;

import java.io.IOException;

public class StarshipViewModel extends AndroidViewModel {

    private MutableLiveData<Starship> starshipMutableLiveData = new MutableLiveData<>();

    public StarshipViewModel(@NonNull Application application) {
        super(application);
    }

    public MutableLiveData<Starship> getStarship(String string) {
        new FetchStarshipTask().execute(string);
        return this.starshipMutableLiveData;

    }

    private class FetchStarshipTask extends AsyncTask<String, Void, Starship> {


        @Override
        protected Starship doInBackground(String... strings) {
            try {
                return new AllRepositories().getStarship(strings[0]);
            } catch (IOException e) {
                return null;
            }
        }

        @Override
        protected void onPostExecute(Starship starship) {
            super.onPostExecute(starship);
;            starshipMutableLiveData.setValue(starship);
        }
    }
}
