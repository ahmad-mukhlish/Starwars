package com.ujian.starwarsapp.model.repositories;

import com.ujian.starwarsapp.model.retrofit.ApiClient;
import com.ujian.starwarsapp.model.retrofit.ApiInterface;

public abstract class BaseRepository {
    protected ApiInterface network;

    protected BaseRepository() {
        network = ApiClient.getApiClient().create(ApiInterface.class);
    }
}
