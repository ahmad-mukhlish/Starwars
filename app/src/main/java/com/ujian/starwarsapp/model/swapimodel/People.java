package com.ujian.starwarsapp.model.swapimodel;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class People {
    @Expose
    @SerializedName("url")
    private java.lang.String url;
    @Expose
    @SerializedName("edited")
    private java.lang.String edited;
    @Expose
    @SerializedName("created")
    private java.lang.String created;
    @Expose
    @SerializedName("starships")
    private List<java.lang.String> starships;
    @Expose
    @SerializedName("vehicles")
    private List<java.lang.String> vehicles;
    @Expose
    @SerializedName("species")
    private List<java.lang.String> species;
    @Expose
    @SerializedName("films")
    private List<java.lang.String> films;
    @Expose
    @SerializedName("homeworld")
    private java.lang.String homeworld;
    @Expose
    @SerializedName("gender")
    private java.lang.String gender;
    @Expose
    @SerializedName("birth_year")
    private java.lang.String birthYear;
    @Expose
    @SerializedName("eye_color")
    private java.lang.String eyeColor;
    @Expose
    @SerializedName("skin_color")
    private java.lang.String skinColor;
    @Expose
    @SerializedName("hair_color")
    private java.lang.String hairColor;
    @Expose
    @SerializedName("mass")
    private java.lang.String mass;
    @Expose
    @SerializedName("height")
    private java.lang.String height;
    @Expose
    @SerializedName("name")
    private java.lang.String name;

    public java.lang.String getUrl() {
        return url;
    }

    public void setUrl(java.lang.String url) {
        this.url = url;
    }

    public java.lang.String getEdited() {
        return edited;
    }

    public void setEdited(java.lang.String edited) {
        this.edited = edited;
    }

    public java.lang.String getCreated() {
        return created;
    }

    public void setCreated(java.lang.String created) {
        this.created = created;
    }

    public List<java.lang.String> getStarships() {
        return starships;
    }

    public void setStarships(List<java.lang.String> starships) {
        this.starships = starships;
    }

    public List<java.lang.String> getVehicles() {
        return vehicles;
    }

    public void setVehicles(List<java.lang.String> vehicles) {
        this.vehicles = vehicles;
    }

    public List<java.lang.String> getSpecies() {
        return species;
    }

    public void setSpecies(List<java.lang.String> species) {
        this.species = species;
    }

    public List<java.lang.String> getFilms() {
        return films;
    }

    public void setFilms(List<java.lang.String> films) {
        this.films = films;
    }

    public java.lang.String getHomeworld() {
        return homeworld;
    }

    public void setHomeworld(java.lang.String homeworld) {
        this.homeworld = homeworld;
    }

    public java.lang.String getGender() {
        return gender;
    }

    public void setGender(java.lang.String gender) {
        this.gender = gender;
    }

    public java.lang.String getBirthYear() {
        return birthYear;
    }

    public void setBirthYear(java.lang.String birthYear) {
        this.birthYear = birthYear;
    }

    public java.lang.String getEyeColor() {
        return eyeColor;
    }

    public void setEyeColor(java.lang.String eyeColor) {
        this.eyeColor = eyeColor;
    }

    public java.lang.String getSkinColor() {
        return skinColor;
    }

    public void setSkinColor(java.lang.String skinColor) {
        this.skinColor = skinColor;
    }

    public java.lang.String getHairColor() {
        return hairColor;
    }

    public void setHairColor(java.lang.String hairColor) {
        this.hairColor = hairColor;
    }

    public java.lang.String getMass() {
        return mass;
    }

    public void setMass(java.lang.String mass) {
        this.mass = mass;
    }

    public java.lang.String getHeight() {
        return height;
    }

    public void setHeight(java.lang.String height) {
        this.height = height;
    }

    public java.lang.String getName() {
        return name;
    }

    public void setName(java.lang.String name) {
        this.name = name;
    }
}
