package com.ujian.starwarsapp.viewmodel;

import android.app.Application;
import android.os.AsyncTask;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.MutableLiveData;

import com.ujian.starwarsapp.model.repositories.AllRepositories;
import com.ujian.starwarsapp.model.swapimodel.Planet;

import java.io.IOException;

public class PlanetViewModel extends AndroidViewModel {

    private MutableLiveData<Planet> planetMutableLiveData = new MutableLiveData<>();

    public PlanetViewModel(@NonNull Application application) {
        super(application);
    }

    public MutableLiveData<Planet> getPlanet(String string) {
        new FetchPlanetTask().execute(string);
        return this.planetMutableLiveData;

    }

    private class FetchPlanetTask extends AsyncTask<String, Void, Planet> {


        @Override
        protected Planet doInBackground(String... strings) {
            try {
                return new AllRepositories().getPlanet(strings[0]);
            } catch (IOException e) {
                return null;
            }
        }

        @Override
        protected void onPostExecute(Planet planet) {
            super.onPostExecute(planet);
;            planetMutableLiveData.setValue(planet);
        }
    }
}
