package com.ujian.starwarsapp.view;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.ujian.starwarsapp.R;
import com.ujian.starwarsapp.databinding.ActivityFilmBinding;
import com.ujian.starwarsapp.model.adapters.LinkAdapter;
import com.ujian.starwarsapp.model.swapimodel.Film;
import com.ujian.starwarsapp.viewmodel.FilmViewModel;

public class FilmActivity extends AppCompatActivity {

    private FilmViewModel filmViewModel;
    private static final String TAG = "FilmActivity";
    private ActivityFilmBinding binding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityFilmBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());
        filmViewModel = ViewModelProviders.of(this).get(FilmViewModel.class);
        observeViewModel();
    }

    private void observeViewModel() {

        java.lang.String alamat = getIntent().getExtras().getString("url");
        filmViewModel.getFilm(alamat).observe(this, new Observer<Film>() {
            @Override
            public void onChanged(final Film film) {
                binding.title.setText(film.getTitle());
                binding.episode.setText(film.getEpisodeId()+"");
                binding.openingCrawl.setText(film.getOpeningCrawl());
                binding.director.setText(film.getDirector());
                binding.producer.setText(film.getProducer());
                binding.releaseDate.setText(film.getReleaseDate());



                binding.rvPeoplesFilm.setLayoutManager(new LinearLayoutManager(FilmActivity.this));
                binding.rvPeoplesFilm.setAdapter(new
                        LinkAdapter(film.getCharacters(),
                        FilmActivity.this,
                        new Intent(FilmActivity.this, PeopleActivity.class)));

                binding.rvPeoplesFilm.setNestedScrollingEnabled(false);

                binding.rvPlanetsFilm.setLayoutManager(new LinearLayoutManager(FilmActivity.this));
                binding.rvPlanetsFilm.setAdapter(new
                        LinkAdapter(film.getPlanets(),
                        FilmActivity.this,
                        new Intent(FilmActivity.this, PlanetActivity.class)));

                binding.rvPlanetsFilm.setNestedScrollingEnabled(false);

                binding.rvStarshipsFilm.setLayoutManager(new LinearLayoutManager(FilmActivity.this));
                binding.rvStarshipsFilm.setAdapter(new
                        LinkAdapter(film.getStarships(),
                        FilmActivity.this,
                        new Intent(FilmActivity.this, StarshipActivity.class)));

                binding.rvStarshipsFilm.setNestedScrollingEnabled(false);


                binding.rvVehiclesFilm.setLayoutManager(new LinearLayoutManager(FilmActivity.this));
                binding.rvVehiclesFilm.setAdapter(new
                        LinkAdapter(film.getVehicles(),
                        FilmActivity.this,
                        new Intent(FilmActivity.this, VehicleActivity.class)));

                binding.rvVehiclesFilm.setNestedScrollingEnabled(false);

                binding.rvSpeciesFilm.setLayoutManager(new LinearLayoutManager(FilmActivity.this));
                binding.rvSpeciesFilm.setAdapter(new
                        LinkAdapter(film.getSpecies(),
                        FilmActivity.this,
                        new Intent(FilmActivity.this, SpeciesActivity.class)));

                binding.rvSpeciesFilm.setNestedScrollingEnabled(false);


                binding.created.setText(film.getCreated());
                binding.edited.setText(film.getEdited());
                binding.url.setText(film.getUrl());
                binding.url.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(FilmActivity.this,FilmActivity.class);
                        intent.putExtra("url",film.getUrl());
                        FilmActivity.this.startActivity(intent);
                    }
                });

            }
        });

    }


}
