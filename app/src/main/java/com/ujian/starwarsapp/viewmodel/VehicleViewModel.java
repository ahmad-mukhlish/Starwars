package com.ujian.starwarsapp.viewmodel;

import android.app.Application;
import android.os.AsyncTask;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.MutableLiveData;

import com.ujian.starwarsapp.model.repositories.AllRepositories;
import com.ujian.starwarsapp.model.swapimodel.Vehicle;

import java.io.IOException;

public class VehicleViewModel extends AndroidViewModel {

    private MutableLiveData<Vehicle> vehicleMutableLiveData = new MutableLiveData<>();

    public VehicleViewModel(@NonNull Application application) {
        super(application);
    }

    public MutableLiveData<Vehicle> getVehicle(String string) {
        new FetchVehicleTask().execute(string);
        return this.vehicleMutableLiveData;

    }

    private class FetchVehicleTask extends AsyncTask<String, Void, Vehicle> {


        @Override
        protected Vehicle doInBackground(String... strings) {
            try {
                return new AllRepositories().getVehicle(strings[0]);
            } catch (IOException e) {
                return null;
            }
        }

        @Override
        protected void onPostExecute(Vehicle vehicle) {
            super.onPostExecute(vehicle);
;            vehicleMutableLiveData.setValue(vehicle);
        }
    }
}
