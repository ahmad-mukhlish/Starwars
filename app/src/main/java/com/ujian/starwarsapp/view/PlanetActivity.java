package com.ujian.starwarsapp.view;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.ujian.starwarsapp.R;
import com.ujian.starwarsapp.databinding.ActivityPlanetBinding;
import com.ujian.starwarsapp.databinding.ActivitySpeciesBinding;
import com.ujian.starwarsapp.model.adapters.LinkAdapter;
import com.ujian.starwarsapp.model.swapimodel.Planet;
import com.ujian.starwarsapp.viewmodel.PlanetViewModel;

public class PlanetActivity extends AppCompatActivity {

    private PlanetViewModel planetViewModel;
    private static final String TAG = "PlanetActivity";
    private ActivityPlanetBinding binding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityPlanetBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());

        planetViewModel = ViewModelProviders.of(this).get(PlanetViewModel.class);
        observeViewModel();
    }

    private void observeViewModel() {

        java.lang.String alamat = getIntent().getExtras().getString("url");
        planetViewModel.getPlanet(alamat).observe(this, new Observer<Planet>() {
            @Override
            public void onChanged(final Planet planet) {
                binding.name.setText(planet.getName());
                binding.rotationPeriod.setText(planet.getRotationPeriod());
                binding.orbitalPeriod.setText(planet.getOrbitalPeriod());
                binding.diameter.setText(planet.getDiameter());
                binding.climate.setText(planet.getClimate());
                binding.gravity.setText(planet.getGravity());
                binding.terrain.setText(planet.getTerrain());
                binding.survaceWater.setText(planet.getSurfaceWater());
                binding.population.setText(planet.getPopulation());

                binding.rvResidentsPlanets.setLayoutManager(new LinearLayoutManager(PlanetActivity.this));
                binding.rvResidentsPlanets.setAdapter(new
                        LinkAdapter(planet.getResidents(),
                        PlanetActivity.this,
                        new Intent(PlanetActivity.this, PeopleActivity.class)));

                binding.rvResidentsPlanets.setNestedScrollingEnabled(false);

                binding.rvFilmsPlanets.setLayoutManager(new LinearLayoutManager(PlanetActivity.this));
                binding.rvFilmsPlanets.setAdapter(new
                        LinkAdapter(planet.getFilms(),
                        PlanetActivity.this,
                        new Intent(PlanetActivity.this, FilmActivity.class)));

                binding.rvFilmsPlanets.setNestedScrollingEnabled(false);




                binding.created.setText(planet.getCreated());
                binding.edited.setText(planet.getEdited());
                binding.url.setText(planet.getUrl());

                binding.url.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(PlanetActivity.this, PlanetActivity.class);
                        intent.putExtra("url",planet.getUrl());
                        PlanetActivity.this.startActivity(intent);
                    }
                });

            }
        });

    }
}
