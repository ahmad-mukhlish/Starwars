package com.ujian.starwarsapp.viewmodel;

import android.app.Application;
import android.os.AsyncTask;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.MutableLiveData;

import com.ujian.starwarsapp.model.repositories.AllRepositories;
import com.ujian.starwarsapp.model.swapimodel.Species;

import java.io.IOException;

public class SpeciesViewModel extends AndroidViewModel {

    private MutableLiveData<Species> speciesMutableLiveData = new MutableLiveData<>();

    public SpeciesViewModel(@NonNull Application application) {
        super(application);
    }

    public MutableLiveData<Species> getSpecies(String string) {
        new FetchSpeciesTask().execute(string);
        return this.speciesMutableLiveData;

    }

    private class FetchSpeciesTask extends AsyncTask<String, Void, Species> {


        @Override
        protected Species doInBackground(String... strings) {
            try {
                return new AllRepositories().getSpecies(strings[0]);
            } catch (IOException e) {
                return null;
            }
        }

        @Override
        protected void onPostExecute(Species species) {
            super.onPostExecute(species);
;            speciesMutableLiveData.setValue(species);
        }
    }
}
