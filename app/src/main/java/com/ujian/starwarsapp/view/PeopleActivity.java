package com.ujian.starwarsapp.view;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.ujian.starwarsapp.databinding.ActivityPeopleBinding;
import com.ujian.starwarsapp.model.adapters.LinkAdapter;
import com.ujian.starwarsapp.model.swapimodel.People;
import com.ujian.starwarsapp.viewmodel.PeopleViewModel;

public class PeopleActivity extends AppCompatActivity {

    private PeopleViewModel peopleViewModel;
    private static final java.lang.String TAG = "PeopleActivity";
    private ActivityPeopleBinding binding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityPeopleBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());
        peopleViewModel = ViewModelProviders.of(this).get(PeopleViewModel.class);
        observeViewModel();

    }

    private void observeViewModel() {

        java.lang.String alamat = getIntent().getExtras().getString("url");
        peopleViewModel.getPeople(alamat).observe(this, new Observer<People>() {
            @Override
            public void onChanged(final People people) {

                binding.name.setText(people.getName());
                binding.height.setText(people.getHeight());
                binding.mass.setText(people.getMass());
                binding.hairColor.setText(people.getHairColor());
                binding.skinColor.setText(people.getSkinColor());
                binding.eyeColor.setText(people.getEyeColor());
                binding.birthYear.setText(people.getBirthYear());
                binding.gender.setText(people.getGender());
                binding.homeworld.setText(people.getHomeworld());
                binding.homeworld.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(PeopleActivity.this, PlanetActivity.class);
                        intent.putExtra("url",people.getHomeworld());
                        PeopleActivity.this.startActivity(intent);
                    }
                });

                binding.rvFilmsPeople.setLayoutManager(new LinearLayoutManager(PeopleActivity.this));
                binding.rvFilmsPeople.setAdapter(new
                        LinkAdapter(people.getFilms(),
                        PeopleActivity.this,
                        new Intent(PeopleActivity.this, FilmActivity.class)));

                binding.rvFilmsPeople.setNestedScrollingEnabled(false);

                binding.rvSpeciesPeople.setLayoutManager(new LinearLayoutManager(PeopleActivity.this));
                binding.rvSpeciesPeople.setAdapter(new
                        LinkAdapter(people.getSpecies(),
                        PeopleActivity.this,
                        new Intent(PeopleActivity.this, SpeciesActivity.class)));

                binding.rvSpeciesPeople.setNestedScrollingEnabled(false);

                binding.rvVehiclesPeople.setLayoutManager(new LinearLayoutManager(PeopleActivity.this));
                binding.rvVehiclesPeople.setAdapter(new
                        LinkAdapter(people.getVehicles(),
                        PeopleActivity.this,
                        new Intent(PeopleActivity.this, VehicleActivity.class)));

                binding.rvVehiclesPeople.setNestedScrollingEnabled(false);


                binding.rvStarshipsPeople.setLayoutManager(new LinearLayoutManager(PeopleActivity.this));
                binding.rvStarshipsPeople.setAdapter(new
                        LinkAdapter(people.getStarships(),
                        PeopleActivity.this,
                        new Intent(PeopleActivity.this, StarshipActivity.class)));

                binding.rvStarshipsPeople.setNestedScrollingEnabled(false);


                binding.created.setText(people.getCreated());
                binding.edited.setText(people.getEdited());
                binding.url.setText(people.getUrl());
                binding.url.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(PeopleActivity.this, PeopleActivity.class);
                        intent.putExtra("url",people.getUrl());
                        PeopleActivity.this.startActivity(intent);
                    }
                });


            }
        });
    }
}
