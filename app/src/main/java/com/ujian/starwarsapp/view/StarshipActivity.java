package com.ujian.starwarsapp.view;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.ujian.starwarsapp.R;
import com.ujian.starwarsapp.databinding.ActivityStarshipBinding;
import com.ujian.starwarsapp.model.adapters.LinkAdapter;
import com.ujian.starwarsapp.model.swapimodel.Starship;
import com.ujian.starwarsapp.viewmodel.StarshipViewModel;

public class StarshipActivity extends AppCompatActivity {

    private StarshipViewModel starshipViewModel;
    private static final String TAG = "StarshipActivity";
    private ActivityStarshipBinding binding ;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityStarshipBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());
        starshipViewModel = ViewModelProviders.of(this).get(StarshipViewModel.class);
        observeViewModel();
    }

    private void observeViewModel() {
        java.lang.String alamat = getIntent().getExtras().getString("url");
        starshipViewModel.getStarship(alamat).observe(this, new Observer<Starship>() {
            @Override
            public void onChanged(final Starship starship) {

                binding.name.setText(starship.getName());
                binding.model.setText(starship.getModel());
                binding.manufacturer.setText(starship.getManufacturer());
                binding.costInCredits.setText(starship.getCostInCredits());
                binding.length.setText(starship.getLength());
                binding.maxAtmospheringSpeed.setText(starship.getMaxAtmospheringSpeed());
                binding.crew.setText(starship.getCrew());
                binding.passangers.setText(starship.getPassengers());
                binding.cargoCapacity.setText(starship.getCargoCapacity());
                binding.consumables.setText(starship.getConsumables());
                binding.hyperdriveRating.setText(starship.getHyperdriveRating());
                binding.mglt.setText(starship.getMglt());
                binding.starshipClass.setText(starship.getStarshipClass());

                binding.rvPilotsStarship.setLayoutManager(new LinearLayoutManager(StarshipActivity.this));
                binding.rvPilotsStarship.setAdapter(new
                        LinkAdapter(starship.getFilms(),
                        StarshipActivity.this,
                        new Intent(StarshipActivity.this, PeopleActivity.class)));

                binding.rvPilotsStarship.setNestedScrollingEnabled(false);

                binding.rvFilmStarship.setLayoutManager(new LinearLayoutManager(StarshipActivity.this));
                binding.rvFilmStarship.setAdapter(new
                        LinkAdapter(starship.getFilms(),
                        StarshipActivity.this,
                        new Intent(StarshipActivity.this, FilmActivity.class)));

                binding.rvFilmStarship.setNestedScrollingEnabled(false);


                binding.created.setText(starship.getCreated());
                binding.edited.setText(starship.getEdited());
                binding.url.setText(starship.getUrl());
                binding.url.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(StarshipActivity.this, StarshipActivity.class);
                        intent.putExtra("url",starship.getUrl());
                        StarshipActivity.this.startActivity(intent);
                    }
                });
                
            }
        });
    }
}
