package com.ujian.starwarsapp.view;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;

import android.os.Bundle;
import android.view.View;

import com.ujian.starwarsapp.databinding.ActivityMainBinding;
import com.ujian.starwarsapp.model.adapters.PeopleAdapter;
import com.ujian.starwarsapp.model.swapimodel.BaseResponse;
import com.ujian.starwarsapp.viewmodel.MainViewModel;

public class MainActivity extends AppCompatActivity {
    private static final String TAG = "MainActivity";
    private ActivityMainBinding binding;
    private MainViewModel viewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        binding = ActivityMainBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());

        viewModel = ViewModelProviders.of(this).get(MainViewModel.class);

        observeViewModel();

    }

    private void observeViewModel() {
        viewModel.fetchBaseResponse().observe(this, new Observer<BaseResponse>() {
            @Override
            public void onChanged(BaseResponse baseResponse) {
                binding.load.setVisibility(View.GONE);
                binding.rvBaseResponse.setVisibility(View.VISIBLE);
                binding.rvBaseResponse.setLayoutManager(new LinearLayoutManager(MainActivity.this));
                binding.rvBaseResponse.setAdapter(new PeopleAdapter(baseResponse.getResults(),MainActivity.this));
            }
        });
    }

}
