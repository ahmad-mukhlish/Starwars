package com.ujian.starwarsapp.viewmodel;

import android.app.Application;
import android.os.AsyncTask;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.MutableLiveData;

import com.ujian.starwarsapp.model.swapimodel.BaseResponse;
import com.ujian.starwarsapp.model.swapimodel.Film;
import com.ujian.starwarsapp.model.repositories.AllRepositories;

import java.io.IOException;

public class MainViewModel extends AndroidViewModel {
    MutableLiveData<BaseResponse> baseResponseLiveData = new MutableLiveData<>();
    MutableLiveData<Film> filmLiveData = new MutableLiveData<>();

    public MainViewModel(@NonNull Application application) {
        super(application);
    }

    public MutableLiveData<BaseResponse> fetchBaseResponse(){
        new FetchBaseResponseTask().execute();
        return baseResponseLiveData;
    }

    private class FetchBaseResponseTask extends AsyncTask<Void,Void, BaseResponse>{

        @Override
        protected BaseResponse doInBackground(Void... voids) {
            try {
                return new AllRepositories().getBaseResponse();
            } catch (IOException e) {
                return null;
            }
        }

        @Override
        protected void onPostExecute(BaseResponse baseResponse) {
            super.onPostExecute(baseResponse);
            baseResponseLiveData.setValue(baseResponse);
        }
    }

    private class FetchFilmTask extends AsyncTask<String,Void, Film>{

        @Override
        protected Film doInBackground(String... strings) {
            try {
                return new AllRepositories().getFilm(strings[0]);
            } catch (IOException e) {
                return null;
            }
        }

        @Override
        protected void onPostExecute(Film film) {
            super.onPostExecute(film);
            filmLiveData.setValue(film);
        }
    }
}
