package com.ujian.starwarsapp.model.swapimodel;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class BaseResponse {

    @Expose
    @SerializedName("results")
    private List<People> results;
    @Expose
    @SerializedName("next")
    private java.lang.String next;
    @Expose
    @SerializedName("count")
    private int count;

    public List<People> getResults() {
        return results;
    }

    public void setResults(List<People> results) {
        this.results = results;
    }

    public java.lang.String getNext() {
        return next;
    }

    public void setNext(java.lang.String next) {
        this.next = next;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }
}
